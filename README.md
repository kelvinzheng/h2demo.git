# H2

## 控制台地址

```aidl
http://127.0.0.1:8088/h2
```

## JDBC URL

- jdbc:h2:~/h2demo;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;AUTO_SERVER=TRUE;

## 启动后测试接口

- http://localhost:8088/get
- http://localhost:8088/add?username=hanmeimei