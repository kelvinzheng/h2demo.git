/**
 * @project h2demo
 * @package com.kelvin.h2.controller
 * @author Administrator
 * @date 2022/3/30 14:47
 */
package com.kelvin.h2.controller;

import com.kelvin.h2.dao.TestDao;
import com.kelvin.h2.entity.TestEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName: TestController
 * @Description:
 * @author Administrator
 * @date 2022/3/30 14:47
 */
@RestController
public class TestController {
    @Resource
    private TestDao testDao;
    @GetMapping("/get")
    public String getUsername() {
        TestEntity query = testDao.query(1);
        String username = "null";
        if (query != null) {
            username = query.getUsername();
        }
        return username;
    }
    @GetMapping("/add")
    public String addUsername(String username) {
        TestEntity query = testDao.query(1);
        Integer id = 1;
        if (query != null) {
            id = query.getId() + 1;
        }
        query.setId(id);
        query.setUsername(username);
        testDao.insert(query);
        return "Added";
    }
}