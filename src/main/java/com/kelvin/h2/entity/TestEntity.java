/**
 * @project h2demo
 * @package com.kelvin.h2.entity
 * @author Administrator
 * @date 2022/3/30 14:50
 */
package com.kelvin.h2.entity;

import lombok.Data;

/**
 * @ClassName: TestEntity
 * @Description:
 * @author Administrator
 * @date 2022/3/30 14:50
 */
@Data
public class TestEntity {
    private Integer id;
    private String username;
}