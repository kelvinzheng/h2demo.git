/**
 * @project h2demo
 * @package com.kelvin.h2.dao
 * @author Administrator
 * @date 2022/3/30 14:48
 */
package com.kelvin.h2.dao;

import com.kelvin.h2.entity.TestEntity;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName: TestDao
 * @Description:
 * @author Administrator
 * @date 2022/3/30 14:48
 */
public interface TestDao {
    void insert(TestEntity item);
    TestEntity query(@Param("id") Integer id);
}