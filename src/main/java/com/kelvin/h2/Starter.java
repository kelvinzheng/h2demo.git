/**
 * @project h2demo
 * @package com.kelvin.h2
 * @author Administrator
 * @date 2022/3/30 14:38
 */
package com.kelvin.h2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName: Starter
 * @Description:
 * @author Administrator
 * @date 2022/3/30 14:38
 */
@ComponentScan(value = {"com.kelvin.*"})
@SpringBootApplication
@MapperScan(basePackages = {"com.kelvin.**.dao"})
public class Starter {
    public static void main(String[] args) {
        SpringApplication.run(Starter.class, args);
    }
}