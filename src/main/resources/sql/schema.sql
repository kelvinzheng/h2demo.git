CREATE TABLE
    IF
    NOT EXISTS test (
    id INT ( 8 ) NOT NULL,
    username VARCHAR ( 512 ) NOT NULL,
    PRIMARY KEY ( id )
    );